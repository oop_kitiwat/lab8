package com.kitkit;



public class TestShape {
    private String name;
    private double width;
    private double height;

    public TestShape(String name, double width, double height) {
        this.name = name;
        this.width = width;
        this.height = height;

    }
    public double getWidth(){
        return width;
    }
    public double getHeight(){
        return height;
    }
    public double Perimeter() {
        double Para = (width + height)*2;
        return Para;
    }

    public double calArea() {
        double result =  (width * height);
        return result;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void print() {
        System.out.println(name + " ");
        System.out.print("Width = " + width + " ");
        System.out.print("Height = " + height + " ");
        System.out.println("Area of a " + name + " " + "is" + ": " + calArea());
        System.out.println("Perimeter of "+name+" is "+ Perimeter());
    }


}
