package com.kitkit;

public class Triangle {
    private String name;
    private double a;
    private double b;
    private double c;

    public Triangle(String name, double a, double b, double c) {
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public String getName() {
        return name;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double triArea() {
        double s = (a + b + c) / 2;
        double tri = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        return tri;
    }

    public double triPerimeter() {
        double triPod = (a + b + c);
        return triPod;
    }

    public void print() {
        System.out.println("Triangle area " + triArea());
        System.out.println("Triangle Perimater is " + triPerimeter());

    }

}
