package com.kitkit;

public class Tree {
    private String name;
    private char symbol;
    private int x;
    private int y;

    public Tree(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;

    }

    public char getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void print() {
        System.out.println("Tree " + name + " X:" + x + " Y:" + y);
    }

}
