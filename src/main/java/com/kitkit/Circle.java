package com.kitkit;

public class Circle {
    private String name;
    private double radius;

    public Circle(String name, double radius) {
        this.name = name;
        this.radius = radius;
    }
    public double getRadius(){
        return radius;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double cirArea() {
        double ra = 3.14 * (radius * radius);
        return ra;
    }

    public double cirPara() {
        double para = 2 * 3.14 * radius;
        return para;
    }

    public void print() {
        System.out.println(name + " ");
        System.out.println("Area of " + name + " is " + cirArea());
        System.out.println("Perimeter of " + name + " is " + cirPara());
    }

}
